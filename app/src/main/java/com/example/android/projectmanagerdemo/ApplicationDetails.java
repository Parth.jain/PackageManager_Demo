package com.example.android.projectmanagerdemo;

import android.content.pm.FeatureInfo;
import android.content.pm.PackageInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ApplicationDetails extends AppCompatActivity {
    TextView mApplicationLabel, mPackageName, mVersion, mFeatures, mPermissions, mTargetVersion,
            mFirstInstalledTime, mLastModify, mPath;
    PackageInfo packageInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application_details);
        mApplicationLabel = findViewById(R.id.applabel);
        mPackageName =  findViewById(R.id.package_name);
        mVersion = findViewById(R.id.version_name);
        mFeatures = findViewById(R.id.req_feature);
        mPermissions =  findViewById(R.id.req_permission);
        mTargetVersion = findViewById(R.id.andversion);
        mPath =  findViewById(R.id.path);
        mFirstInstalledTime =  findViewById(R.id.insdate);
        mLastModify = findViewById(R.id.last_modify);

        AppData appData = (AppData) getApplicationContext();
        packageInfo = appData.getPackageInfo();

        setValues();
    }

    private void setValues() {
        mApplicationLabel.setText(getPackageManager().getApplicationLabel(
                packageInfo.applicationInfo));

        mPackageName.setText(packageInfo.packageName);

        mVersion.setText(packageInfo.versionName);

        mTargetVersion.setText(Integer
                .toString(packageInfo.applicationInfo.targetSdkVersion));

        mPath.setText(packageInfo.applicationInfo.sourceDir);

        mFirstInstalledTime.setText(setDateFormat(packageInfo.firstInstallTime));

        mLastModify.setText(setDateFormat(packageInfo.lastUpdateTime));

        if (packageInfo.reqFeatures != null)
            mFeatures.setText(getFeatures(packageInfo.reqFeatures));
        else
            mFeatures.setText("-");

        if (packageInfo.requestedPermissions != null)
            mPermissions
                    .setText(getPermissions(packageInfo.requestedPermissions));
        else
            mPermissions.setText("-");
    }
    private String setDateFormat(long time) {
        Date date = new Date(time);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String strDate = formatter.format(date);
        return strDate;
    }

    private String getPermissions(String[] requestedPermissions) {
        String permission = "";
        for (int i = 0; i < requestedPermissions.length; i++) {
            permission = permission + requestedPermissions[i] + ",\n";
        }
        return permission;
    }
    private String getFeatures(FeatureInfo[] reqFeatures) {
        String features = "";
        for (int i = 0; i < reqFeatures.length; i++) {
            features = features + reqFeatures[i] + ",\n";
        }
        return features;
    }
}
