package com.example.android.projectmanagerdemo;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    PackageManager packageManager;
    ListView mApplicationList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        packageManager=getPackageManager();

        List<PackageInfo> packageList=packageManager.getInstalledPackages(packageManager
                .GET_PERMISSIONS);

        List<PackageInfo> packageList1=new ArrayList<PackageInfo>();

        //filter system apps

        for(PackageInfo Pi:packageList){
            boolean b=isSystemPackage(Pi);
            if(!b){
                packageList1.add(Pi);
            }
        }
        mApplicationList=findViewById(R.id.applicationList);
        mApplicationList.setAdapter(new ApplicationAdapter(this,packageList1,packageManager));

        mApplicationList.setOnItemClickListener(this);
    }

    private boolean isSystemPackage(PackageInfo packageInfo) {
    return ((packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM)!=0)?true:false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PackageInfo packageInfo=(PackageInfo) parent.getItemAtPosition(position);
        AppData appData = (AppData) getApplicationContext();
        appData.setPackageInfo(packageInfo);

        Intent appInfo=new Intent(MainActivity.this,ApplicationDetails.class);
        startActivity(appInfo);
    }

}
