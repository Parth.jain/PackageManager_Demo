package com.example.android.projectmanagerdemo;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.hardware.SensorEventListener;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by parth on 14/3/18.
 */

public class ApplicationAdapter extends BaseAdapter {
    List<PackageInfo> packageList;
    Activity context;
    PackageManager packageManager;

    public ApplicationAdapter(Activity context, List<PackageInfo> packageList, PackageManager
            packageManager) {
        super();
    this.context=context;
    this.packageList=packageList;
    this.packageManager=packageManager;
    }

    @Override
    public int getCount() {
        return packageList.size();
    }

    @Override
    public Object getItem(int position) {
        return packageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        LayoutInflater inflater=context.getLayoutInflater();
        viewHolder =new ViewHolder();

        if(convertView==null){
            convertView=inflater.inflate(R.layout.list_item,null);

            viewHolder.apkName=(TextView)convertView.findViewById(R.id.application_name);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder= (ViewHolder) convertView.getTag();
        }
        PackageInfo packageInfo= (PackageInfo) getItem(position);
        Drawable application_icon=packageManager.getApplicationIcon(packageInfo.applicationInfo);
        String application_name=packageManager.getApplicationLabel(packageInfo.applicationInfo)
                .toString();
        application_icon.setBounds(0,0,40,40);
        viewHolder.apkName.setCompoundDrawables(application_icon,null,null,null);
        viewHolder.apkName.setCompoundDrawablePadding(15);
        viewHolder.apkName.setText(application_name);

        return convertView;
    }

    private class ViewHolder {
        TextView apkName;
    }

}
