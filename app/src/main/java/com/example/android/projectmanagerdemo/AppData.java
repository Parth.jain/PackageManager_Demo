package com.example.android.projectmanagerdemo;

import android.app.Application;
import android.content.pm.PackageInfo;

/**
 * Created by parth on 14/3/18.
 */

public class AppData extends Application {
    PackageInfo packageInfo;

    public PackageInfo getPackageInfo(){
        return packageInfo;
    }

    public void setPackageInfo(PackageInfo packageInfo){
        this.packageInfo=packageInfo;
    }
}
